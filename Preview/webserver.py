import os, sys
import BaseHTTPServer
import CGIHTTPServer
import cgitb; cgitb.enable()
 
server = BaseHTTPServer.HTTPServer
handler = CGIHTTPServer.CGIHTTPRequestHandler
webdir = '.'
port = 8010
os.chdir(webdir)
srvraddr = ('', port)
httpd = server(srvraddr, handler)
httpd.serve_forever()