dbfilename = 'profile-file'
ENDDB = 'enndb.'
ENDREC = 'endrec.'
RECSEP = '=>'


def storeDbase(db, dbfilename = dbfilename):
    """
    save db in file
    """
    dbfile = open(dbfilename,'w')
    for key in db:
        print >> dbfile, key
        for (name,value) in db[key].items():
            print >> dbfile, name + RECSEP + repr(value)
        print >> dbfile, ENDREC
    print >> dbfile, ENDDB
    dbfile.close()

def loadDbase(dbfilename = dbfilename):
    """
    read data from file
    """
    dbfile = open(dbfilename)
    import sys
    sys.stdin = dbfile
    db = {}
    key = dbfile.readline().rstrip('\n')
    while key.rstrip('\n') != ENDDB:
        key = key.rstrip('\n')
        rec = {}
        field = dbfile.readline().rstrip('\n')
        while field.rstrip('\n') != ENDREC:
            field = field.rstrip('\n')
            name, value = field.split(RECSEP)
            rec[name] = eval(value)
            field = dbfile.readline()
        db[key] = rec
        key = dbfile.readline()
    return db

if __name__ == "__main__":
    from initdata import db
    storeDbase(db)


