import shelve
fieldname = ('name','age','job','pay')
maxfield = max(len(f) for f in fieldname)
db = shelve.open('class-shelve')

while True:
    key = raw_input('\nKey? => ')

    if not key: break
    try:
        record = db[key]
    except:
        print key
    else:
        for field in fieldname:
            print field.ljust(maxfield), '=>', getattr(record, field)