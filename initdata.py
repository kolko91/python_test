# NAME, AGE, PAY = range(3)
# bob = ['Bob Smith', 42, 30000, 'software']
# sue = ['Sue Jones', 45, 40000, 'hardware']
# persons = [bob,sue]
# pays = [person[2] for person in persons]
# print pays
#
# pays_summ = sum(person[2] for person in persons)
# print pays_summ
# print bob[NAME]

# bob = [['name', 'Bob Smith'], ['age', 42]]
# sue = [['name', 'Sue Jones'], ['age', 45]]
# people = [bob, sue]
# 
# 
# def field(record, label):
#     for(fname, fvalue) in record:
#         if(fname == label):
#             return fvalue


# print field(bob,'name')

# bob = {'name': 'Bob', 'age': 42}
# sue = {'name': 'Sue', 'age': 45}
# print bob

# bob = dict(name='Bob', age=42)
# # print bob
#
# name = ['name', 'age']
# values = ['Name', 42]
#
# print dict(zip(name, values))
#
# fields = ('name','age','job')
# record = dict.fromkeys(fields,'?')
# print record

# peoples = [bob, sue]
# for person in peoples:
#     print(person['name'], person['age'])


# names = [person['name'] for person in peoples]
# names = list(map((lambda x: x['name']),peoples))
# print names

# g = (rec['name'] for rec in peoples if rec['age'] >= 45)
# print next(g)

bob = {'name': 'Bob Smith', 'age': 42, 'pay': 30000, 'job': 'dev'}
sue = {'name': 'Sue Jones', 'age': 45, 'pay': 40000, 'job': 'hdw'}
tom = {'name': 'Tom', 'age': 50, 'pay': 0, 'job': None}

db = {}
db['bob'] = bob
db['sue'] = sue
db['tom'] = tom

if __name__ == '__main__':
    for key in db:
        print key," =>\n ", db[key]