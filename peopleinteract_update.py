import shelve
from person_start import Person
fieldname = ('name','age','job','pay')

db = shelve.open('class-shelve')
while True:
    key = raw_input('\nKey? => ')

    if not key: break
    if key in db:
        record = db[key]
    else:
        record = Person(name = '?',age = '?')
    for field in fieldname:
        currval = getattr(record,field)
        newtext = raw_input('\t[%s]=%s\n\t\tnew?=>' % (field, currval))
        if newtext:
            setattr(record,field,eval(newtext))
    db[key] = record
db.close()
